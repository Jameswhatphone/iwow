import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { NgxMaskModule } from 'ngx-mask';

import { FilterPipe } from './shared/pipes/filter.pipe';

import { environment } from 'src/environments/environment';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { SearchInsuranceComponent } from './components/shared/search-insurance/search-insurance.component';
import { CardCampaignComponent } from './components/shared/card-campaign/card-campaign.component';
import { CampaignCardListComponent } from './components/shared/campaign-card-list/campaign-card-list.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { AuthModalComponent } from './components/shared/auth-modal/auth-modal.component';
import { SearchResultComponent } from './components/search-result/search-result.component';
import { InsuranceCardComponent } from './components/shared/insurance-card/insurance-card.component';
import { CustomerInfoComponent } from './components/insurance-detail/customer-info/customer-info.component';
import { CarInfoComponent } from './components/insurance-detail/car-info/car-info.component';
import { InsuranceSummaryComponent } from './components/insurance-summary/insurance-summary.component';
import { PaymentStatusComponent } from './components/payment-status/payment-status.component';
import { InsuranceDetailComponent } from './components/insurance-detail/insurance-detail.component';
import { InsuranceInfoComponent } from './components/insurance-detail/insurance-info/insurance-info.component';
import { MemberComponent } from './components/member/member.component';
import { InfoComponent } from './components/member/info/info.component';
import { HistoryComponent } from './components/member/history/history.component';
import { ListResultsComponent } from './components/search-result/list-results/list-results.component';
import { CompareResultsComponent } from './components/search-result/compare-results/compare-results.component';
import { ToastComponent } from './components/shared/toast/toast.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { RefundPolicyComponent } from './components/refund-policy/refund-policy.component';
import { TermConditionComponent } from './components/term-condition/term-condition.component';
import { TermInsuranceComponent } from './components/term-insurance/term-insurance.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    FilterPipe,
    SearchInsuranceComponent,
    CardCampaignComponent,
    CampaignCardListComponent,
    FooterComponent,
    AuthModalComponent,
    SearchResultComponent,
    InsuranceCardComponent,
    CustomerInfoComponent,
    CarInfoComponent,
    InsuranceSummaryComponent,
    PaymentStatusComponent,
    InsuranceDetailComponent,
    InsuranceInfoComponent,
    MemberComponent,
    InfoComponent,
    HistoryComponent,
    ListResultsComponent,
    CompareResultsComponent,
    ToastComponent,
    PrivacyPolicyComponent,
    RefundPolicyComponent,
    TermConditionComponent,
    TermInsuranceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    NgxWebstorageModule.forRoot(),
    NgxMaskModule.forRoot(),
    BrowserAnimationsModule,
    MatProgressBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
