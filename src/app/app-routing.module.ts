import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SearchResultComponent } from './components/search-result/search-result.component';
import { InsuranceDetailComponent } from './components/insurance-detail/insurance-detail.component';
import { InsuranceInfoComponent } from './components/insurance-detail/insurance-info/insurance-info.component';
import { CustomerInfoComponent } from './components/insurance-detail/customer-info/customer-info.component';
import { CarInfoComponent } from './components/insurance-detail/car-info/car-info.component';
import { InsuranceSummaryComponent } from './components/insurance-summary/insurance-summary.component';
import { PaymentStatusComponent } from './components/payment-status/payment-status.component';
import { MemberComponent } from './components/member/member.component';
import { InfoComponent } from './components/member/info/info.component';
import { HistoryComponent } from './components/member/history/history.component';
import { ListResultsComponent } from './components/search-result/list-results/list-results.component';
import { CompareResultsComponent } from './components/search-result/compare-results/compare-results.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { RefundPolicyComponent } from './components/refund-policy/refund-policy.component';
import { TermConditionComponent } from './components/term-condition/term-condition.component';
import { TermInsuranceComponent } from './components/term-insurance/term-insurance.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'result',
    redirectTo: '/result/lists',
    pathMatch: 'full'
  },
  {
    path: 'result',
    component: SearchResultComponent,
    children: [
      {
        path: 'lists',
        component: ListResultsComponent
      },
      {
        path: 'compare',
        component: CompareResultsComponent
      }
    ]
  },
  {
    path: 'member',
    redirectTo: '/member/info',
    pathMatch: 'full'
  },
  {
    path: 'member',
    component: MemberComponent,
    children: [
      {
        path: 'info',
        component: InfoComponent
      },
      {
        path: 'history',
        component: HistoryComponent
      }
    ]
  },
  {
    path: 'insurance',
    redirectTo: '/insurance/insurance-detail',
    pathMatch: 'full'
  },
  {
    path: 'insurance',
    component: InsuranceDetailComponent,
    children: [
      {
        path: 'insurance-detail',
        component: InsuranceInfoComponent
      },
      {
        path: 'customer-detail',
        component: CustomerInfoComponent
      },
      {
        path: 'car-detail',
        component: CarInfoComponent
      }
    ]
  },
  {
    path: 'summary',
    component: InsuranceSummaryComponent
  },
  {
    path: 'payment/:token/:status',
    component: PaymentStatusComponent
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent
  },
  {
    path: 'refund-policy',
    component: RefundPolicyComponent
  },
  {
    path: 'term',
    component: TermConditionComponent
  },
  {
    path: 'term-insurance',
    component: TermInsuranceComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true, onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
