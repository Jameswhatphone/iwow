import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map, concat, concatMap, switchMap, tap } from 'rxjs/operators';
import { Observable, timer, interval, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private CREATE_SESSION_URL = `${environment.apiUrl}/search/createsession`;
  private SEARCH_URL = `${environment.apiUrl}/search`;

  private searchResult = new BehaviorSubject<any>([]);

  private headers = { 
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    }) 
  };

  constructor(
    private httpClient: HttpClient
  ) { }

  createSearchSession(body: { brandId: number, family: string, makeCode: string, manufactureYear: number, engineSize: number }) {
    if (body) {
      return this.httpClient.post(this.CREATE_SESSION_URL, body, this.headers).pipe(
        map(res => res['result']['sessionId'])
      );
    }
  }

  getSearch(sessionId: string) {
    if (sessionId) {
      return this.httpClient.get(`${this.SEARCH_URL}/${sessionId}`, this.headers).pipe(
        map(res => res['result'])
      );
    }
  }

  getSearchResult(): Observable<{ coverages: any[], numberOfProgressDone: string, numberOfWorkToBeComplete: string }> {
    return this.searchResult.asObservable();
  }

  setSearchResult(result: { coverages: any[], numberOfProgressDone: string, numberOfWorkToBeComplete: string }): void {
    this.searchResult.next(result);
  }
}
