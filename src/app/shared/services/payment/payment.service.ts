import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FbAuthService } from '../firebase/fb-auth.service';
import { map } from 'rxjs/operators';
import { LocalStorageService } from 'ngx-webstorage';

// declare let Omise: any;

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  private LOCAL_API_URL = 'https://web-api.iwowthailand.com';

  constructor(
    private httpClient: HttpClient,
    private fbAuth: FbAuthService,
    private localStorage: LocalStorageService
  ) {}

  async createOrder(data: any) {
    const url = `${environment.apiUrl}/coverage/createorder`;
    const header = { headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    })};
    return new Promise((resolve, reject) => {
      try {
        this.httpClient.post(url, data, header)
          .pipe(
            map(item => item['result'])
          )
          .subscribe(res => resolve(res), err => reject(err))
      } catch (err) {
        reject(err);
      }
    });
  }

  checkout(paymentData: any, userToken: string) {
    const url = `${environment.apiUrl}/coverage/checkout`;
    const header = { headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'tokenSession': userToken
    })}
    return new Promise((resolve, reject) => {
      try {
        this.httpClient.post(url, paymentData, header)
          .pipe(
            map(res => res['result'])
          )
          .subscribe(res => resolve(res), err => reject(err))
      } catch (err) {
        reject(err);
      }
    });
  }

  getOmiseToken(card: any) {
    const url = `${this.LOCAL_API_URL}/createToken`;
    const headers = { headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }) }
    return this.httpClient.post(url, card, headers)
    .pipe(map(token => token['id']));
  }
}
