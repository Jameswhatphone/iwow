import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getAllCampaign() {
    const url = `${environment.apiUrl}/campaign/all`;
    const headers = { headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })};
    return this.httpClient.get(url, headers)
      .pipe(map(item => item['results']))
      .toPromise();
  }
}
