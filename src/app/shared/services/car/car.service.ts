import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CarBrand } from '../../data-classes/car-brand.model';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CarModel } from '../../data-classes/car-model.model';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  private CAR_BRAND_URL = `${environment.apiUrl}/vehicle/getbrand`;
  private CAR_MODEL_URL = `${environment.apiUrl}/vehicle/getmodel`;
  private headers = { 
    headers: new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Cache-Control', 'no-cache')
    };

  constructor(
    private httpClient: HttpClient
  ) { }

  getCarBrandList(): Observable<CarBrand> {
    return this.httpClient.get(this.CAR_BRAND_URL, this.headers)
      .pipe(
        map(item => item['results'] as CarBrand)
      );
  }

  getCarModelList(id?: number): Observable<CarModel> {
    let url: string;
    if (id) {
      url = `${this.CAR_MODEL_URL}?brandId=${id}`;
    } else {
      url = this.CAR_MODEL_URL;
    }
    return this.httpClient.get(url, this.headers)
        .pipe(
          map(item => item['results'] as CarModel)
        );
  }
}
