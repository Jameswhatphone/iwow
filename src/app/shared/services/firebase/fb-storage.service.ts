import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class FbStorageService {

  constructor(
    private afStorage: AngularFireStorage
  ) { }

  getPartnerImg() {
    return this.afStorage.ref('partner').getDownloadURL();
  }
}
