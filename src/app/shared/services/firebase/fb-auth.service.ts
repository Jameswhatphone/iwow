import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { LocalStorageService } from 'ngx-webstorage';
import { auth } from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class FbAuthService {

  protected authToken: string;

  constructor(
    private afAuth: AngularFireAuth,
    private localStorage: LocalStorageService
  ) {
    this.localStorage.observe('token').subscribe(token => {
      this.authToken = token;
      this.localStorage.store('token', token);
    });
  }

  isRememberme(status: boolean, email: string, token: string) {
    if (status) {
      this.localStorage.store('email', email);
      this.localStorage.store('token', token);
    }
  }

  signUpWithEmail(user: { email: string, password: string }) {
    if (!user) {
      console.log('no user');
    }
    return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
  }

  signInWithEmail(user: { email: string, password: string }) {
    if (!user) {
      console.log('no user');
    }
    return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);
  }

  signInWithCustomToken() {
    if (!this.authToken) {
      console.log('no token');
    }
    return this.afAuth.auth.signInWithCustomToken(this.authToken);
  }

  signInWithFacebook() {
    return this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider());
  }

  getSocialRedirectToken() {
    return this.afAuth.auth.getRedirectResult();
  }

  signOut() {
    return this.afAuth.auth.signOut();
  }

  getCurrentUser() {
    return this.afAuth.authState;
  }
}
