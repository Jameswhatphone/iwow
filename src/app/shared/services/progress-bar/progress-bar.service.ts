import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgressBarService {

  private progressMode: BehaviorSubject<string>;
  private progressValue: BehaviorSubject<number>;
  private isShow: boolean = false;

  constructor() {}

  getMode(): Observable<string> {
    return this.progressMode.asObservable();
  }

  setMode(mode: string = 'determinate'): void {
    this.progressMode.next(mode);
  }

  getValue(): Observable<number> {
    return this.progressValue.asObservable();
  }

  setValue(min: number = 0, max: number = 0): void {
    this.progressValue.next((min/max)*100);
  }

  isLoading(): boolean {
    return this.isShow;
  }

  onLoading(mode: string, min?: number, max?: number): void {
    this.isShow = true;
    this.setMode(mode);
    if (min && max) {
      this.setValue(min, max);
    }
  }

  onLoaded(): void {
    this.isShow = false;
  }
}
