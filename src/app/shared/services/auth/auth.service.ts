import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map, tap } from 'rxjs/operators';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) { }

  async registerWithFirebaseToken(token: string, body: {
    email: string,
    firstName: string,
    lastName: string,
    mobileNo: string,
    facebookId?: string
  }) {
    const url = `${environment.apiUrl}/member/register`;
    const header = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('tokenSession', token)
        .set('Cache-Control', 'no-cache')
      };
    return await new Promise((resolve, reject) => {
      try {
        this.httpClient.post(url, body, header).subscribe(result => {
          if (result) { resolve(result); }
        }, err => reject(err));
      } catch (err) {
        reject(err);
      }
    });
  }

  async getMemberInfo(token: string) {
    const url = `${environment.apiUrl}/member/getInfo`;
    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        tokenSession: token
      })
    };

    return await new Promise((resolve, reject) => {
      try {
        this.httpClient.get(url, header)
        .pipe(map((item: any) => item.result))
        .subscribe(res => {
          if (res) { resolve(res); }
        }, err => {
          reject(err);
        });
      } catch (err) {
        reject(err);
      }
    });
  }

  async updateMemberInfo(token: string, memberData: any) {
    const url = `${environment.apiUrl}/member/update`;
    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        tokenSession: token,
        'Cache-Control': 'no-cache'
      })
    };

    return await new Promise((resolve, reject) => {
      try {
        this.httpClient.put(url, memberData, header).subscribe(res => {
          if (res) { resolve(res); }
        }, err => reject(err));
      } catch (err) {
        reject(err);
      }
    });
  }

  async getMemberHistory(token: string, id?: any) {
    let url: string;
    if (id) {
      url = `${environment.apiUrl}/member/history/id/${id}`;
    } else {
      url = `${environment.apiUrl}/member/history/list/all`;
    }
    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        tokenSession: token
      })
    };

    return await new Promise((resolve, reject) => {
      try {
        this.httpClient.get(url, header)
        .pipe(
          // tslint:disable-next-line
          map((item: any) => item.results),
          tap(list => {
            for (const item of list) {
              item.effectiveDate_text = moment(item.effectiveDate).locale('th').format('ll');
              item.expirationDate_text = moment(item.expirationDate).locale('th').format('ll');
            }
          })
        )
        .subscribe(res => {
          if (res) { resolve(res); }
        }, err => reject(err));
      } catch (err) {
        reject(err);
      }
    });
  }
}
