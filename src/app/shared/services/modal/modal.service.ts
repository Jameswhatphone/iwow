import { Injectable } from '@angular/core';

declare let $: any;

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  protected isRegister: boolean;

  constructor() {
    this.isRegister = false;
  }

  get isRegisterModal() {
    return this.isRegister = true;
  }

  get isLoginModal() {
    return this.isRegister = false;
  }

  get getModalStatus() {
    return this.isRegister;
  }

  showModal() {
    $('#authModal').modal('show');
  }

  hideModal() {
    $('#authModal').modal('hide');
  }
}
