import { Injectable } from '@angular/core';
declare let $: any;

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  private message: string;

  constructor() { }

  setToastMessage(type: string, insuranceName?: string) {
    switch (type) {
      case 'compare':
        this.message = `คุณได้เลือกประกัน ${insuranceName} แล้ว เปรียบเทียบได้เลย!`;
      default:
        this.message = 'Welcome to iWOW';
    }
  }

  getToastMessage(): string {
    return this.message;
  }

  showToast(): void {
    $('.toast').toast('show');
  }
}
