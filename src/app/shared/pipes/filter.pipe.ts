import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items: any[], key: string, value: any): any {
    if (!items) { return []; }
    if (!value) { return items; }

    return items.filter(item => {
      return item[key].indexOf(value) !== -1;
    });
  }

}
