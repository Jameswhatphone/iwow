import { CarModel } from './car-model.model';

export class CarBrand {
    brandId: number;
    makeCode: string;
    shortName: string;
    longName: string;
    description: string;
    isActive: boolean;
    models: CarModel[] = [];
}
