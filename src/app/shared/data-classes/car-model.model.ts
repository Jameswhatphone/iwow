export class CarModel {
    modelId: number;
    shortName: string;
    engineSize: number;
    family: string;
    description: string;
    manufactureYear: number;
    vehicleKey: string;
}
