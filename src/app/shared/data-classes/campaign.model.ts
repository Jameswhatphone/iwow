export class Campaign {
    articleId: number;
    campaignId: number;
    campaignLimit: number;
    description: string;
    endDate: string;
    isActive: boolean;
    name: string;
    startDate: string;
}
