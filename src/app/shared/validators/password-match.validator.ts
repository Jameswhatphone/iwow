import { AbstractControl } from '@angular/forms';

export class PasswordMatch {
    static matchPassword(ac: AbstractControl) {
        let password = ac.get('regPassword').value;
        let confirmPassword = ac.get('confirmPassword').value;

        if (password != confirmPassword) {
            console.log('false');
            ac.get('confirmPassword').setErrors({ MatchPassword: true });
        }
    }
}
