import { Component } from '@angular/core';
import { AuthService } from './shared/services/auth/auth.service';
import { ProgressBarService } from './shared/services/progress-bar/progress-bar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  
  constructor(
    protected authSrv: AuthService
  ) {
    // authSrv.getCurrentUser().subscribe((res) => console.log(res), (err) => console.log(err));
  }
}
