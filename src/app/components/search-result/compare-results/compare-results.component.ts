import { Component, OnInit } from '@angular/core';
import { SessionStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-compare-results',
  templateUrl: './compare-results.component.html',
  styleUrls: ['./compare-results.component.sass']
})
export class CompareResultsComponent implements OnInit {

  compareList: any;

  constructor(
    private sessionStorage: SessionStorageService,
    private router: Router
  ) {}

  ngOnInit() {
    this.compareList = this.sessionStorage.retrieve('compareList')
    // this.sessionStorage.observe('compareList').subscribe(list => {
    //   console.log(list)
    //   this.compareList = list;
    // });
  }

  navigateWithData(result: any) {
    this.sessionStorage.store('selectedResult', result);
    this.router.navigate(['/insurance'], { state: { data: result, navigationId: 123 }});
  }

}
