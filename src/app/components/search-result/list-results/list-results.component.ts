import { Component, OnInit, DoCheck, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search/search.service';
import { SessionStorageService, LocalStorageService } from 'ngx-webstorage';
import { Location } from '@angular/common';
import { ToastService } from 'src/app/shared/services/toast/toast.service';

declare let $: any;

@Component({
  selector: 'app-list-results',
  templateUrl: './list-results.component.html',
  styleUrls: ['./list-results.component.sass']
})
export class ListResultsComponent implements OnInit, DoCheck, OnDestroy {

  results: any = [];
  insuranceType1: any;
  insuranceType2Plus: any;
  insuranceType2: any;
  insuranceType3Plus: any;
  insuranceType3: any;
  car = this.localStorage.retrieve('carSearch') || {};

  isLoading = false;

  subscription: Subscription;

  private SEARCH_RESULTS = 'searchResults';
  compareList: any[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private searchSrv: SearchService,
    private sessionStorage: SessionStorageService,
    private location: Location,
    private toastSrv: ToastService,
    private localStorage: LocalStorageService
  ) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.fetch();
  }

  ngDoCheck() {
    this.fetch();
  }

  fetch() {
    this.subscription = this.searchSrv.getSearchResult().subscribe((item: any = {}) => {
      if (item instanceof Object) {
        if (parseInt(item.numberOfProgressDone, 10) >= parseInt(item.numberOfWorkToBeComplete, 10)) {
          this.isLoading = false;
        } else if (parseInt(item.numberOfProgressDone, 10) < parseInt(item.numberOfWorkToBeComplete, 10)) {
          this.isLoading = true;
        } else {
          this.isLoading = false;
        }
        if (item.coverages) {
          this.results = item.coverages;
          this.sessionStorage.store(this.SEARCH_RESULTS, item);
        } else {
          this.results = [];
        }
      } else {
        this.isLoading = false;
      }
    }, err => console.log(err));
    if (this.results.length > 0) {
      this.updateValue();
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  goBack() {
    this.router.navigate(['/home']);
  }

  updateValue() {
    this.insuranceType1 = this.results.filter((item: { policyClassId: any; }) => parseInt(item.policyClassId, 10) === 1);
    this.insuranceType2Plus = this.results.filter((item: { policyClassId: any; }) => parseInt(item.policyClassId, 10) === 2);
    this.insuranceType2 = this.results.filter((item: { policyClassId: any; }) => parseInt(item.policyClassId, 10) === 3);
    this.insuranceType3Plus = this.results.filter((item: { policyClassId: any; }) => parseInt(item.policyClassId, 10) === 4);
    this.insuranceType3 = this.results.filter((item: { policyClassId: any; }) => parseInt(item.policyClassId, 10) === 5);
  }

  navigateWithData(result: any) {
    this.sessionStorage.store('selectedResult', result);
    this.router.navigate(['/insurance'], { state: { data: result, navigationId: 123 } });
  }

  addCompareList(item: any) {
    if (this.compareList.length > 0) {
      const tmp = this.compareList.find(itm => itm.raw === item.raw);
      if (tmp) {
        this.compareList.splice(this.compareList.indexOf(item), 1);
        this.sessionStorage.store('compareList', this.compareList);
      } else {
        this.compareList.push(item);
        this.sessionStorage.store('compareList', this.compareList);
        alert(`คุณได้เลือกประกัน ${item.insuranceName} แล้ว เปรียบเทียบเลย!`);
        // this.toastSrv.showToast();
      }
    } else {
      this.compareList.push(item);
      this.sessionStorage.store('compareList', this.compareList);
      // this.toastSrv.showToast();
      alert(`คุณได้เลือกประกัน ${item.insuranceName} แล้ว เลือกอีกแบบเพื่อเปรียบเทียบ`);
    }
  }

  hideModal(name: string, result: any) {
    this.navigateWithData(result)
    $(`#${name}`).modal('hide');
  }

}
