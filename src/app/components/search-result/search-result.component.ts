import { Component, OnInit, OnDestroy, OnChanges, AfterContentChecked, SimpleChanges, DoCheck } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchService } from 'src/app/shared/services/search/search.service';
import { SessionStorageService, LocalStorageService } from 'ngx-webstorage';
import { Observable, Subscription } from 'rxjs';
import { Location } from '@angular/common';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.sass']
})
export class SearchResultComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {}

}
