import { Component, OnInit } from '@angular/core';
import { Campaign } from 'src/app/shared/data-classes/campaign.model';
import { CampaignService } from 'src/app/shared/services/campaign/campaign.service';
import { LocalStorageService } from 'ngx-webstorage';
import { FbAuthService } from 'src/app/shared/services/firebase/fb-auth.service';

declare let $: any;

@Component({
  selector: 'app-campaign-card-list',
  templateUrl: './campaign-card-list.component.html',
  styleUrls: ['./campaign-card-list.component.sass']
})
export class CampaignCardListComponent implements OnInit {

  // token: string = '';
  // campaign_data: Campaign[] = [];
  campaign_data: any[] = [];

  constructor(
    private campaignSrv: CampaignService,
    private localStorage: LocalStorageService,
    private fbAuthSrv: FbAuthService
  ) {}

  ngOnInit() {
    this.carousel();
    // this.campaignSrv.getAllCampaign()
    //   .then(list => {
    //     this.campaign_data = list;
    //   })
    //   .catch(err => {
    //     console.log(err);
    //     alert(`get campaign list: ${err}`);
    //   })

    this.campaign_data = [
      {
        name: "iWOW x Siamsport",
        image: "../../../../assets/images/wowxsiam1.svg"
      },
      {
        name: "iWOW x Golf Channal",
        image: "../../../../assets/images/wowxgolf.svg"
      },
      {
        name: "iWOW x Siamsport",
        image: "../../../../assets/images/wowxsiam2.svg"
      }
      
    ]
  }

  carousel() {
    // Auto Slide
    $('#myCarousel').carousel({
      interval: 5000
    });

    // Carousel Control
    $('.next').click(function () {
      $('.carousel').carousel('next');
      return false;
    });
    $('.prev').click(function () {
      $('.carousel').carousel('prev');
      return false;
    });

    // onSlide
    $("#myCarousel").on("slide.bs.carousel", function (e) {
      var $e = $(e.relatedTarget);
      var idx = $e.index();
      var itemsPerSlide = 3;
      var totalItems = $(".carousel-item").length;
      if (idx >= totalItems - (itemsPerSlide - 1)) {
        var it = itemsPerSlide -
          (totalItems - idx);
        for (var i = 0; i < it; i++) {
          // append slides to end 
          if (e.direction == "left") {
            $(
              ".carousel-item").eq(i).appendTo(".carousel-inner");
          } else {
            $(".carousel-item").eq(0).appendTo(".carousel-inner");
          }
        }
      }
    });
  }

}
