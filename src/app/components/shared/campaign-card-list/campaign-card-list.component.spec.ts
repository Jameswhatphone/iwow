import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignCardListComponent } from './campaign-card-list.component';

describe('CampaignCardListComponent', () => {
  let component: CampaignCardListComponent;
  let fixture: ComponentFixture<CampaignCardListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignCardListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
