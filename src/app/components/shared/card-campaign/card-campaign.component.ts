import { Component, OnInit, Input } from '@angular/core';
import { Campaign } from '../../../shared/data-classes/campaign.model';

@Component({
  selector: 'app-card-campaign',
  templateUrl: './card-campaign.component.html',
  styleUrls: ['./card-campaign.component.sass']
})
export class CardCampaignComponent implements OnInit {
  @Input() props: any;
  @Input() index: number;

  constructor() { }

  ngOnInit() {
  }

}
