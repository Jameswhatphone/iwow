import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ModalService } from 'src/app/shared/services/modal/modal.service';
import { FbAuthService } from 'src/app/shared/services/firebase/fb-auth.service';
import { SessionStorageService, LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  user: firebase.User;

  constructor(
    public modalSrv: ModalService,
    private fbAuthSrv: FbAuthService,
    private sessionStorage: SessionStorageService,
    private localStorage: LocalStorageService,
    private router: Router
  ) { }

  ngOnInit() {
    this.fbAuthSrv.getCurrentUser().subscribe((fbUser: firebase.User) => {
      console.log(fbUser);
      if (fbUser) {
        this.user = fbUser;
        this.localStorage.store('user', fbUser);
        fbUser.getIdToken().then(token => this.localStorage.store('userToken', token)).catch(err => console.log(err));
      }
    }, err => console.log(err));
  }

  signOut() {
    this.fbAuthSrv.signOut()
      .finally(() => {
        this.localStorage.clear();
        this.sessionStorage.clear();
        this.router.navigate(['/home']);
        this.user = null;
      });
  }

}
