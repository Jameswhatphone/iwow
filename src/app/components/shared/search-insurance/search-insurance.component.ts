import { Component, OnInit } from '@angular/core';
import { CarBrand } from 'src/app/shared/data-classes/car-brand.model';
import { CarModel } from 'src/app/shared/data-classes/car-model.model';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { ModalService } from 'src/app/shared/services/modal/modal.service';
import { CarService } from 'src/app/shared/services/car/car.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SearchService } from 'src/app/shared/services/search/search.service';
import { SessionStorageService } from 'ngx-webstorage';
import { Subscription } from 'rxjs';
import provinceData from 'src/assets/data_province.json';
import coverageClassData from 'src/assets/data_coverage_class.json';

interface IProvince {
  id: number;
  name: string;
  code: string;
  acronym: string;
  name_en: string;
}

interface ICoverageClass {
  id: number;
  name: string;
  description: string;
  is_active: number;
}
@Component({
  selector: 'app-search-insurance',
  templateUrl: './search-insurance.component.html',
  styleUrls: ['./search-insurance.component.sass']
})

export class SearchInsuranceComponent implements OnInit {

  title: string;
  carBrand: number;
  carModel: number;
  yearModel: number;
  brandData: CarBrand;
  modelData: CarModel;
  yearData: number[];
  searchForm: FormGroup;
  searchResult: { coverages: any[], numberOfProgressDone: string, numberOfWorkToBeComplete: string };
  subscription: Subscription;

  mode: string;
  value: number;
  color = 'accent';
  isProgressShow = false;
  showSearch = true;

  isLoading = false;
  provinces: IProvince[] = provinceData;
  coverageClasses: ICoverageClass[] = coverageClassData;

  private SEARCH_SESSION_ID = 'searchSessionId';
  private SEARCH_RESULTS = 'searchResults';
  private CAR_BRAND = 'carBrand';
  private CAR_MODEL = 'carModel';
  private YEAR_MODEL = 'yearModel';
  private PROVINCE_SEARCH = 'provinceSearch';

  selectedCarBrand: CarBrand;
  selectedCarModel: CarModel;
  selectedCoverage: ICoverageClass[] = [this.coverageClasses[0]];

  constructor(
    private router: Router,
    protected modalSrv: ModalService,
    private carSrv: CarService,
    private searchSrv: SearchService,
    private fb: FormBuilder,
    private sessionStorage: SessionStorageService
  ) {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        // console.log(val)
        switch (val.urlAfterRedirects) {
          case '/home': {
            this.showSearch = true;
            return this.title = 'สะดวก เร็ว จบได้ใน 3 นาที';
          }
          case '/result/lists': {
            this.showSearch = true;
            // console.log(this.searchResult)
            // tslint:disable-next-line: max-line-length
            return this.title = this.searchResult.coverages ? `เราเจอทั้งหมด ${this.searchResult.coverages.length} แบบประกัน` : 'เราไม่เจอแบบประกันที่เหมาะกับรถคุณ';
          }
          case '/result/compare': {
            this.showSearch = true;
            // console.log(this.searchResult)
            // tslint:disable-next-line: max-line-length
            return this.title = this.searchResult.coverages ? `เราเจอทั้งหมด ${this.searchResult.coverages.length} แบบประกัน` : 'เราไม่เจอแบบประกันที่เหมาะกับรถคุณ';
          }
          default: this.showSearch = false;
        }
      }
    });
  }

  ngOnInit() {
    this.carSrv.getCarBrandList()
    .subscribe((res) => {
      this.brandData = res;
    }, (err) => {
      console.log('get car brand', err);
      // alert(`get car brand ${err}`);
    });

    this.searchSrv.getSearchResult().subscribe(item => {
      this.searchResult = item;
    }, err => console.log(err));
    // if (!this.sessionStorage.retrieve('searchResults')) {
    // } else {
    //   this.searchResult = this.sessionStorage.retrieve(this.SEARCH_RESULTS);
    // }

    this.searchForm = this.fb.group({
      carBrand: ['', [Validators.required]],
      carModel: ['', [Validators.required]],
      yearModel: ['', [Validators.required]],
      provinceSearch: ['', [Validators.required]]
    });
  }

  get getCarBrand() { return this.searchForm.get(this.CAR_BRAND); }
  get getCarModel() { return this.searchForm.get(this.CAR_MODEL); }
  get getYearModel() { return this.searchForm.get(this.YEAR_MODEL); }
  get getProvinceSearch() { return this.searchForm.get(this.PROVINCE_SEARCH); }

  detectCarBrand() {
    if (this.getCarBrand.value !== '') {
      this.carSrv.getCarModelList(this.getCarBrand.value.brandId)
        .subscribe((res) => {
          this.modelData = res;
        }, (err) => {
          console.log('get car model', err);
          // alert(`get car model ${err}`);
        });
    }
    this.searchForm.patchValue({
      carModel: '',
      yearModel: '',
      provinceSearch: ''
    });
  }

  detectCarModel() {
    if (this.getCarModel.value !== '') {
      this.yearData = this.getCarModel.value.yearGroups.split(',');
    }
  }

  detectCoverageClass(event: any, cc: ICoverageClass) {
    if (event.target.checked) {
      if (this.selectedCoverage.find(item => item.id === cc.id) === undefined) {
        this.selectedCoverage.push(cc);
      }
    } else {
      if (this.selectedCoverage.find(item => item.id === cc.id) !== undefined) {
        const index = this.selectedCoverage.indexOf(cc);
        this.selectedCoverage.splice(index, 1);
      }
    }
  }

  getCoverageClass() {
    if (this.selectedCoverage && this.selectedCoverage.length > 0) {
      const coverageClass: string[] = [];
      for (const item of this.selectedCoverage) {
        coverageClass.push(item.name);
      }
      return coverageClass;
    }
    return null;
  }

  submit() {
    this.searchResult = undefined;
    this.searchSrv.setSearchResult(undefined);
    this.sessionStorage.clear(this.SEARCH_RESULTS);
    this.sessionStorage.clear(this.SEARCH_SESSION_ID);
    const body = {
      brandId: this.getCarBrand.value.brandId,
      family : this.getCarModel.value.family,
      makeCode : this.getCarBrand.value.makeCode,
      manufactureYear : this.getYearModel.value,
      engineSize : this.getCarModel.value.engineSize,
      fuelName: this.getCarModel.value.fuelType,
      provinceId: this.getProvinceSearch.value.id,
      policyType: this.getCoverageClass()
    };
    const carData = {
      carBrand: this.getCarBrand.value,
      carModel: this.getCarModel.value,
      yearModel: this.getYearModel.value,
      province: this.getProvinceSearch.value
    };
    this.sessionStorage.store('carSearch', carData);
    this.onLoading('query');
    this.isLoading = true;
    this.searchSrv.createSearchSession(body)
    .subscribe((res) => {
      this.sessionStorage.store(this.SEARCH_SESSION_ID, res);
    }, (err) => {
      this.isLoading = false;
      this.onLoaded();
      console.log('create search session', err);
    }, () => {
      this.onLoaded();
      const id = this.sessionStorage.retrieve(this.SEARCH_SESSION_ID);
      let interval: any;
      interval = setInterval(() => {
        // tslint:disable-next-line: max-line-length
        if (this.searchResult === undefined || (parseInt(this.searchResult.numberOfProgressDone, 10) !== parseInt(this.searchResult.numberOfWorkToBeComplete, 10))) {
          // tslint:disable-next-line: max-line-length
          this.searchSrv.getSearch(id).subscribe((res: { coverages: any[], numberOfProgressDone: string, numberOfWorkToBeComplete: string }) => {
            // console.log(res);
            if (this.searchResult !== undefined) {
              // tslint:disable-next-line: max-line-length
              this.onLoading('determinate', parseInt(this.searchResult.numberOfProgressDone, 10), parseInt(this.searchResult.numberOfWorkToBeComplete, 10));
            }
            this.searchResult = res;
            this.sessionStorage.store(this.SEARCH_RESULTS, res);
            this.searchSrv.setSearchResult(this.searchResult);
            this.router.navigate(['/result']);

          }, err => {
            this.isLoading = false;
            console.log('get search result', err);
            // alert(`get search result: ${err}`);
            clearInterval(interval);
            this.onLoaded();
          });
        } else if (parseInt(this.searchResult.numberOfProgressDone, 10) >= parseInt(this.searchResult.numberOfWorkToBeComplete, 10)) {
          // console.log('not null');
          // console.log(this.searchResult);
          clearInterval(interval);
          this.isLoading = false;
          this.onLoaded();
          // tslint:disable-next-line: max-line-length
          this.title = this.searchResult && this.searchResult.coverages.length > 0 ? `เราเจอทั้งหมด ${this.searchResult.coverages.length} แบบประกัน` : 'เราไม่เจอแบบประกันที่เหมาะกับรถคุณ';
        }
      }, 3000);
    });
  }

  onLoading(mode: string, min?: number, max?: number) {
    this.isProgressShow = true;
    this.mode = mode;
    if (min && max) {
      this.value = (min / max) * 100;
    } else {
      this.value = 0;
    }
  }

  onLoaded() {
    this.isProgressShow = false;
    this.value = 0;
  }
}
