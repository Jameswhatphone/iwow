import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/shared/services/modal/modal.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { PasswordMatch } from 'src/app/shared/validators/password-match.validator';
import { FbAuthService } from 'src/app/shared/services/firebase/fb-auth.service';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';

declare let $: any

@Component({
  selector: 'app-auth-modal',
  templateUrl: './auth-modal.component.html',
  styleUrls: ['./auth-modal.component.sass']
})
export class AuthModalComponent implements OnInit {

  loginForm: FormGroup;
  registerForm: FormGroup;
  isRememberme = false;

  isLoading = false;

  constructor(
    public modalSrv: ModalService, 
    private fb: FormBuilder,
    private fbAuthSrv: FbAuthService,
    private authSrv: AuthService,
    private router: Router,
    private localStorage: LocalStorageService
  ) {}

  ngOnInit() {
    this.loginForm = this.fb.group({
      loginEmail: ['', [Validators.required, Validators.email]],
      loginPassword: ['', [Validators.required, Validators.minLength(8)]]
    });
    this.registerForm = this.fb.group({
      regEmail: ['', [Validators.required, Validators.email]],
      regPassword: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(8)]]
    }, { validators: PasswordMatch.matchPassword });
  }

  get loginEmail() { return this.loginForm.get('loginEmail'); }
  get loginPassword() { return this.loginForm.get('loginPassword'); }

  get regEmail() { return this.registerForm.get('regEmail'); }
  get regPassword() { return this.registerForm.get('regPassword'); }
  get confirmPassword() { return this.registerForm.get('confirmPassword'); }

  authLogin() {
    const user = {
      email: this.loginEmail.value,
      password: this.loginPassword.value
    };
    // console.log(user);
    if (!user) {
      alert('Something wrong! please check your email or password');
    }

    // if (this.isRememberme) {
      
    // }

    this.isLoading = true;
    this.fbAuthSrv.signInWithEmail(user)
      .then((res) => console.log(res))
      .catch((error) => {
        console.log(error);
        alert('Failed to Sign in! Please try again.');
        this.isLoading = false;
      })
      .finally(() => {
        this.isLoading = false;
        this.loginForm.reset();
        this.modalSrv.hideModal();
      });
  }

  authLoginWithFacebook() {
    this.fbAuthSrv.signInWithFacebook()
      .then((res) => console.log(res))
      .catch((err) => console.log(err))
      .finally(() => {
        this.modalSrv.hideModal();
      })
  }

  authRegisterWithFacebook() {
    this.fbAuthSrv.signInWithFacebook()
    .then((res: any) => {
      console.log(res);
      this.registerIwowWithToken(res.user, res.additionalUserInfo.profile);
    })
    .catch((err) => console.log(err))
    .finally(() => {
      this.modalSrv.hideModal();
    })
  }

  authRegister() {
    const user = {
      email: this.regEmail.value,
      password: this.regPassword.value
    }
    // console.log(user);
    if (!user) {
      alert('Something wrong! please check your email or password');
    }

    this.isLoading = true;
    this.fbAuthSrv.signUpWithEmail(user)
      .then((res) => {
        this.registerIwowWithToken(res.user);
      })
      .catch((error) => {
        console.log(error);
        alert('Failed to Sign up! Please try again.')
        this.isLoading = false;
      })
      .finally(() => {
        this.isLoading = false;
        this.registerForm.reset();
        this.modalSrv.hideModal();
      });
  }

  async registerIwowWithToken(user: firebase.User, facebookUser?: any) {
    let token: string;
    await user.getIdToken()
      .then(result => token = result)
      .catch(err => console.log(err));
    const name = user.displayName ? user.displayName.split(' ') : null;
    const reqBody = {
      email: user.email,
      firstName: name ? name[0] : '',
      lastName: name ? name[1] : '',
      mobileNo: user.phoneNumber || '',
      facebookId: facebookUser ? facebookUser.id : ''
    };
    this.authSrv.registerWithFirebaseToken(token, reqBody)
      .catch(err => console.log(err))
  }

  checkIsRememberMe(event: any) {
    if (event.target.checked) {
      this.isRememberme = true;
    } else {
      this.isRememberme = false;
    }
  } 

}
