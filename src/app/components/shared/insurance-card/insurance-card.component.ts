import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { SessionStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-insurance-card',
  templateUrl: './insurance-card.component.html',
  styleUrls: ['./insurance-card.component.sass']
})
export class InsuranceCardComponent implements OnInit {
  @Input() result: any;

  compareList: any[] = [];

  constructor(
    private sessionStorage: SessionStorageService,
    private router: Router
  ) { }

  ngOnInit() {
    if (this.sessionStorage.retrieve('compareList')) {
      this.compareList = this.sessionStorage.retrieve('compareList');
      console.log(this.compareList)
    }
  }

  navigateWithData() {
    this.sessionStorage.store('selectedResult', this.result);
    this.router.navigate(['/insurance'], { state: { data: this.result, navigationId: 123 }});
  }

  addCompareList(item: any) {
    if (this.compareList.length > 0) {

    } else {
      this.compareList.push(item);
      // console.log(this.compareList);
      this.sessionStorage.store('compareList', this.compareList);
    }
  }

}
