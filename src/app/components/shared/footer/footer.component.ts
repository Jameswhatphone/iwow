import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {

  // Social
  FACEBOOK_LINK = 'https://www.facebook.com/iwow';
  FACEBOOK_IMG = 'assets/images/facebook.png';
  LINE_LINK = 'https://www.facebook.com/iwow';
  LINE_IMG = 'assets/images/line.png';

  // Payment Method
  VISA_IMG = 'assets/images/visa.png';
  MASTERCARD_IMG = 'assets/images/mastercard.png';
  JCB_IMG = 'assets/images/jcb.png';

  // Certificate
  OIC_LOGO = 'assets/images/oic.svg';


  constructor() { }

  ngOnInit() {
  }

}
