import { Component, Renderer2, OnInit, ViewChild } from '@angular/core';
import { SessionStorageService, LocalStorageService } from 'ngx-webstorage';
import * as moment from 'moment';
import { Router, ActivatedRoute } from '@angular/router';
import { PaymentService } from 'src/app/shared/services/payment/payment.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { state } from '@angular/animations';
// import { BeforeOpenEvent, SwalComponent } from '@sweetalert2/ngx-sweetalert2';
// import Swal from 'sweetalert2'

declare let $: any;

@Component({
  selector: 'app-insurance-summary',
  templateUrl: './insurance-summary.component.html',
  styleUrls: ['./insurance-summary.component.sass']
})
export class InsuranceSummaryComponent implements OnInit {

  // @ViewChild('loadingSwal', null) private loadingSwal: SwalComponent;

  cardForm: FormGroup;
  userToken: string;

  insurance: any;
  customerData: any;
  coverageData: any;
  carData: any;
  orderData: any;

  isCheck = false;
  isOic = false;

  yearList = [];

  OMISE_LOGO = '../../../assets/images/omise.png';

  constructor(
    private sessionStorage: SessionStorageService,
    private router: Router,
    private route: ActivatedRoute,
    private paymentSrv: PaymentService,
    private fb: FormBuilder,
    private localStorage: LocalStorageService
  ) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.insurance = this.sessionStorage.retrieve('selectedResult');
    this.customerData = this.sessionStorage.retrieve('customerInfo');
    this.coverageData = this.sessionStorage.retrieve('coverageInfo');
    this.carData = this.sessionStorage.retrieve('carSearch');
    this.orderData = this.sessionStorage.retrieve('order');
    this.userToken = this.localStorage.retrieve('userToken');

    this.customerData.birthdate = moment(this.customerData.birthdate).locale('th').format('LL');
    this.coverageData.startInsurance = moment(this.coverageData.assuredVehicle.startCoverDate).locale('th').format('LL');

    this.yearList.push(moment().year());
    for (let i = 1; i <= 9; i++) {
      this.yearList.push(moment().add(i, 'y').year());
      if (i === 9) { console.log(this.yearList); }
    }

    this.cardForm = this.fb.group({
      cardNumber: ['', [Validators.required, Validators.pattern('[0-9]{16}'), Validators.maxLength(16)]],
      cardName: ['', [Validators.required]],
      expiredMonth: ['', [Validators.required]],
      expiredYear: ['', Validators.required],
      cvc: ['', [Validators.required, Validators.maxLength(3)]]
    });
  }

  cardFormItem(name: string) { return this.cardForm.get(name); }

  isConfirmCheck(event: any) {
    if (event.target.checked) {
      this.isCheck = true;
    } else {
      this.isCheck = false;
    }
  }
  isOicCheck(event: any) {
    if (event.target.checked) {
      this.isOic = true;
    } else {
      this.isOic = false;
    }
  }

  submit() {
    // todo: check authurize if user not login, user can't submit form
    const user = this.localStorage.retrieve('user');
    console.log('user', user);
    if (user != null) {
      let checkoutBody: any = {};
      const card = {
        name: this.cardForm.value.cardName,
        number: this.cardForm.value.cardNumber,
        security_code: this.cardForm.value.cvc,
        expiration_year: this.cardForm.value.expiredYear,
        expiration_month: this.cardForm.value.expiredMonth
      };
      this.paymentSrv.getOmiseToken(card).subscribe(token => {
        checkoutBody = { transactionID: this.orderData.transactionId, chargeToken: token };
        console.log(checkoutBody);
      }, err => console.log(err), () => {
        this.paymentSrv.checkout(checkoutBody, this.userToken)
          .then((res: any) => {
            $('#omiseModal').modal('hide');
            // this.loadingSwal.fire().then(e => {
            this.router.navigate(
              [`/payment/${res.transactionID}/${res.status === 1 ? 'success' : 'failed'}`],
              { state: { payload: res } }
            );
            // })
            // .catch(swalErr => console.log(swalErr));
          })
          .catch(err => console.log(err));
      });
    } else {
      $('#authModal').modal('show');
    }
  }

  checkAuth() {
    const user = this.localStorage.retrieve('user');
    if (!user) {
      $('#authModal').modal('show');
      $('#omiseModal').modal('hide');
    } else {
      $('#authModal').modal('hide');
      $('#omiseModal').modal('show');
    }
  }
}
