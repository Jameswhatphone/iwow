import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import * as moment from 'moment';
import { PaymentService } from 'src/app/shared/services/payment/payment.service';

@Component({
  selector: 'app-car-info',
  templateUrl: './car-info.component.html',
  styleUrls: ['./car-info.component.sass']
})
export class CarInfoComponent implements OnInit {

  customerData: any;
  insurance: any;
  coverageData: any;
  carData: any;
  carForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private sessionStorage: SessionStorageService,
    private paymentSrv: PaymentService
  ) { }

  ngOnInit() {
    this.insurance = this.sessionStorage.retrieve('selectedResult');
    this.customerData = this.sessionStorage.retrieve('customerInfo');
    this.carData = this.sessionStorage.retrieve('carSearch');
    this.carForm = this.fb.group({
      startInsurance: [moment().format('YYYY-MM-DD'), [Validators.required]],
      endInsurance: [moment().add(6, 'months').format('YYYY-MM-DD'), [Validators.required]],
      startCover: [moment().format('YYYY-MM-DD'), [Validators.required]],
      endCover: [moment().add(6, 'months').format('YYYY-MM-DD'), [Validators.required]],
      licensePlate: ['', [Validators.required]],
      regLicenseProvince: ['กรุงเทพมหานคร', [Validators.required]],
      licenseYear: ['', [Validators.required]],
      chasisId: ['', [Validators.required]],
    });
  }

  carFormItem(name: string) { return this.carForm.get(name); }

  submit() {

    const orderData = {
      assured: {
        prefix: this.customerData.prefix,
        firstName: this.customerData.firstname,
        lastName: this.customerData.lastname,
        gender: this.customerData.sex,
        birthDay: parseInt(moment(this.customerData.birthdate).locale('th').format('x'), 10),
        idCardNo: this.customerData.personalId,
        mobilePhone: this.customerData.phone,
        email: this.customerData.email
      },
      address: {
        address: this.customerData.address,
        street: this.customerData.road,
        subDistrict: this.customerData.subdistrict,
        district: this.customerData.district,
        province: this.customerData.province,
        zipcode: this.customerData.postalCode
      },
      addressForBilling: {
        address: this.customerData.billingAddress.address,
        street: this.customerData.billingAddress.road,
        subDistrict: this.customerData.billingAddress.subdistrict,
        district: this.customerData.billingAddress.district,
        province: this.customerData.billingAddress.province,
        zipcode: this.customerData.billingAddress.postalCode
      },
      assuredVehicle: {
        chassisNo: this.carForm.value.chasisId,
        licensePlateNo: this.carForm.value.licensePlate,
        licenseYear: this.carForm.value.licenseYear,
        licenseProvince: this.carForm.value.regLicenseProvince,
        startCoverDate: parseInt(moment(this.carForm.value.startCover).locale('th').format('x'), 10),
        endCoverDate: parseInt(moment(this.carForm.value.endCover).locale('th').format('x'), 10)
      },
      isCombineWithCompulsory: this.insurance.isCombineWithCompulsory,
      selectedCoverage: this.insurance
    }
    this.sessionStorage.store('coverageInfo', orderData);
    this.paymentSrv.createOrder(orderData).then(res => {
      this.sessionStorage.store('order', res);
      console.log(res);
    }).catch(err => {
      console.log(err);
    }).finally(() => {
      this.router.navigate(['/summary'], { relativeTo: this.route });
    })
    console.log(orderData);
  }

}
