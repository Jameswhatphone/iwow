import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { FbAuthService } from 'src/app/shared/services/firebase/fb-auth.service';
import { SessionStorageService } from 'ngx-webstorage';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

declare let $: any;

@Component({
  selector: 'app-customer-info',
  templateUrl: './customer-info.component.html',
  styleUrls: ['./customer-info.component.sass']
})
export class CustomerInfoComponent implements OnInit {

  customerForm: FormGroup;
  userData: firebase.User;
  otp = '';
  firstname: string;
  lastname: string;

  isSameAddess: boolean;
  isDisable = true;

  constructor(
    private fb: FormBuilder,
    private fbAuth: FbAuthService,
    private sessionStorage: SessionStorageService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.customerForm = this.fb.group({
      prefix: ['mr', [Validators.required]],
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      sex: ['male', [Validators.required]],
      birthdate: [moment().subtract('years', 20).format('YYYY-MM-DD'), [Validators.required]],
      phone: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      personalId: ['', [Validators.required]],
      address: ['', [Validators.required]],
      road: ['', [Validators.required]],
      province: ['', [Validators.required]],
      district: ['', [Validators.required]],
      subdistrict: ['', [Validators.required]],
      postalCode: ['', [Validators.required]],
      billingAddress: this.fb.group({
        address: ['', [Validators.required]],
        road: ['', [Validators.required]],
        province: ['', [Validators.required]],
        district: ['', [Validators.required]],
        subdistrict: ['', [Validators.required]],
        postalCode: ['', [Validators.required]],
      })
    });
    this.fbAuth.getCurrentUser()
    .subscribe((fbUser: firebase.User) => {
      this.userData = fbUser;
      this.firstname = this.userData.displayName ? this.userData.displayName.split(' ')[0] : '';
      this.lastname = this.userData.displayName ? this.userData.displayName.split(' ')[1] : '';
      this.customerForm.patchValue({
        firstname: this.firstname,
        lastname: this.lastname,
        email: this.userData.email,
        phone: this.userData.phoneNumber ? this.userData.phoneNumber : ''
      });
    }, err => console.log(err));

    // $.Thailand({
    //   $district: $('#subdistrict'),
    //   $amphoe: $('#district'),
    //   $province: $('#province'),
    //   $zipcode: $('#postalCode')
    // });

  }

  customerFormItem(name: string) { return this.customerForm.get(name); }

  isCheckUpdate(event: any) {
    if (event.target.checked) {
      this.isSameAddess = true;
      this.customerForm.patchValue({
        billingAddress: {
          address: this.customerForm.value.address,
          road: this.customerForm.value.road,
          province: this.customerForm.value.province,
          district: this.customerForm.value.district,
          subdistrict: this.customerForm.value.subdistrict,
          postalCode: this.customerForm.value.postalCode,
        }
      });
    } else {
      this.isSameAddess = false;
      this.customerForm.patchValue({
        billingAddress: {
          address: '',
          road: '',
          province: '',
          district: '',
          subdistrict: '',
          postalCode: '',
        }
      });
    }
  }

  otpChange(event) {
    if (event.target.value) {
      this.otp = event.target.value;
    }
  }

  submit() {
    this.sessionStorage.store('customerInfo', this.customerForm.value);
    this.router.navigate(['../car-detail'], { relativeTo: this.route });
  }

  checkOTP() {
    const customSwal = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-darkgreen text-white',
        cancelButton: 'btn btn-light',
      },
      buttonsStyling: false
    });
    const phone = this.customerFormItem('phone').value;
    const otpTemp = phone.substring(6, 10);
    if (this.otp && this.otp === otpTemp) {
      customSwal.fire({
        title: 'Success!',
        text: 'ลงทะเบียนสำเร็จ!',
        icon: 'success'
      }).then(() => $('#otpModal').modal('hide')).then(() => this.submit());
    } else if (this.otp && this.otp !== otpTemp) {
      customSwal.fire({
        title: 'Failed!',
        text: 'ลงทะเบียนไม่สำเร็จ!',
        icon: 'error'
      }).then(() => $('#otpModal').modal('hide'));
    }
  }

}
