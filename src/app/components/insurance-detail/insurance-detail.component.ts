import { Component, OnInit } from '@angular/core';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-insurance-detail',
  templateUrl: './insurance-detail.component.html',
  styleUrls: ['./insurance-detail.component.sass']
})
export class InsuranceDetailComponent implements OnInit {
  // insuranceImage = '../../../../assets/images/bki.png';
  insurance: any;

  constructor(
    private sessionStorage: SessionStorageService
  ) { }

  ngOnInit() {
    this.insurance = this.sessionStorage.retrieve('selectedResult');
  }

}
