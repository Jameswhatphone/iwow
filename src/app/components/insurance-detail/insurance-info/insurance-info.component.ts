import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-insurance-info',
  templateUrl: './insurance-info.component.html',
  styleUrls: ['./insurance-info.component.sass']
})
export class InsuranceInfoComponent implements OnInit {

  insurance: any;

  constructor(
    private router: Router,
    private sessionStorage: SessionStorageService
  ) { }

  ngOnInit() {
    this.insurance = this.sessionStorage.retrieve('selectedResult');
    this.insurance.isCombineWithCompulsory = false;
  }

  isCheck(event) {
    if (event.target.checked) {
      this.insurance.isCombineWithCompulsory = true;
    } else {
      this.insurance.isCombineWithCompulsory = false;
    }
    this.sessionStorage.store('selectedResult', this.insurance);
  }

  navigateToRoute() {
    if (parseInt(this.insurance.policyType, 10) === 1) {
      return 'https://firebasestorage.googleapis.com/v0/b/iwow-460fc.appspot.com/o/documents%2FCoverage-Type-1.pdf?alt=media';
    } else if (parseInt(this.insurance.policyType, 10) === 2) {
      return 'https://firebasestorage.googleapis.com/v0/b/iwow-460fc.appspot.com/o/documents%2FCoverage-Type-2.pdf?alt=media';
    } else if (parseInt(this.insurance.policyType, 10) === 3) {
      return 'https://firebasestorage.googleapis.com/v0/b/iwow-460fc.appspot.com/o/documents%2FCoverage-Type-3.pdf?alt=media';
    } else {
      return 'https://firebasestorage.googleapis.com/v0/b/iwow-460fc.appspot.com/o/documents%2FCoverage-Type-5.pdf?alt=media';
    }
  }

}
