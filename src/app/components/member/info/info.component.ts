import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LocalStorageService } from 'ngx-webstorage';
import * as moment from 'moment';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { FbAuthService } from 'src/app/shared/services/firebase/fb-auth.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.sass']
})
export class InfoComponent implements OnInit {

  memberForm: FormGroup;
  memberData: any;
  user: firebase.User;
  token: string;

  isLoading: boolean = false;

  constructor(
    private fb: FormBuilder,
    private localStorage: LocalStorageService,
    private authSrv: AuthService,
    private fbAuth: FbAuthService
  ) { }

  ngOnInit() {
    this.memberForm = this.fb.group({
      personalId: ['', [Validators.maxLength(13)]],
      prefix: ['mr'],
      firstname: [''],
      lastname: [''],
      phone: ['', [Validators.maxLength(10)]],
      birthdate: [moment().format('YYYY-MM-D')],
      address: [''],
      road: [''],
      province: ['กรุงเทพมหานคร'],
      subdistrict: [''],
      district: [''],
      postalCode: [''],
      email: ['']
    });

    this.fbAuth.getCurrentUser().subscribe((fbUser: firebase.User) => {
      this.user = fbUser;
      fbUser.getIdToken()
        .then(fbToken => {
          this.token = fbToken;
        })
        .catch(err => console.log(err))
        .finally(() => {
          // console.log(this.token);
          this.authSrv.getMemberInfo(this.token)
          .then(member => {
            this.memberData = member;
            console.log(member);
          })
          .catch(err => console.log(err))
          .finally(() => {
            console.log(this.memberData)
            this.memberForm.patchValue({
              personalId: this.memberData.idCardNo || '',
              prefix: this.memberData.prefix || 'mr',
              firstname: this.memberData.firstName || '',
              lastname: this.memberData.lastName || '',
              phone: this.memberData.mobileNo || '',
              birthdate: moment(this.memberData.birthDay).format('YYYY-MM-D') || moment().format('YYYY-MM-D'),
              address: this.memberData.address || '',
              road: this.memberData.road || '',
              province: this.memberData.province || 'กรุงเทพมหานคร',
              subdistrict: this.memberData.subDistrict || '',
              district: this.memberData.district || '',
              postalCode: this.memberData.zipCode || '',
              email: this.user.email || ''
            });
          });
        });
    }, err => console.log(err), () => {
      
    })
  }

  get personalId() { return this.memberForm.get('personalId') }
  get prefix() { return this.memberForm.get('prefix') }
  get firstname() { return this.memberForm.get('firstname') }
  get lastname() { return this.memberForm.get('lastname') }
  get phone() { return this.memberForm.get('phone') }
  get birthdate() { return this.memberForm.get('birthdate') }
  get address() { return this.memberForm.get('address') }
  get road() { return this.memberForm.get('road') }
  get province() { return this.memberForm.get('province') }
  get subdistrict() { return this.memberForm.get('subdistrict') }
  get district() { return this.memberForm.get('district') }
  get postalCode() { return this.memberForm.get('postalCode') }
  get email() { return this.memberForm.get('email') }

  submit() {
    this.isLoading = true;
    const member = this.memberForm.value;
    const body = {
      email: member.email,
      firstName: member.firstname,
      lastName: member.lastname,
      mobileNo: member.phone,
      idCardNo: member.personalId,
      prefix: member.prefix,
      birthDay: moment(member.birthdate).locale('th').format('x'),
      address: member.address,
      road: member.road,
      province: member.province,
      subDistrict: member.subdistrict,
      district: member.district,
      zipCode: member.postalCode,
      gender: member.prefix === 'mr' ? 'male':'female'
    }
    console.log(body)
    this.authSrv.updateMemberInfo(this.token, body)
      .then(res => console.log(res))
      .catch(err => {
        this.isLoading = false;
        console.log(err);
      })
      .finally(() => {
        this.isLoading = false;
        alert('Saved!');
      })
  }

}
