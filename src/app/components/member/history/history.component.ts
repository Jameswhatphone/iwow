import { Component, OnInit } from '@angular/core';
import { SessionStorageService } from 'ngx-webstorage';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { FbAuthService } from 'src/app/shared/services/firebase/fb-auth.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.sass']
})
export class HistoryComponent implements OnInit {

  user: firebase.User;
  token: string;
  historyList: any[];

  constructor(
    private sessionStorage: SessionStorageService,
    private authSrv: AuthService,
    private fbAuth: FbAuthService
  ) { }

  ngOnInit() {
    this.fbAuth.getCurrentUser().subscribe((fbUser: firebase.User) => {
      fbUser.getIdToken()
      .then(fbToken => this.token = fbToken)
      .catch(err => console.log(err))
      .finally(() => {
        this.authSrv.getMemberHistory(this.token)
        .then((list: any[]) => {
          this.historyList = list;
        })
        .catch(err => console.log(err))
      });
    }, err => console.log(err));
  }

}
