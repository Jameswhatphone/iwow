import { Component, OnInit } from '@angular/core';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-payment-status',
  templateUrl: './payment-status.component.html',
  styleUrls: ['./payment-status.component.sass']
})
export class PaymentStatusComponent implements OnInit {

  customerData: any;
  insurance: any;
  coverageData: any;
  carData: any;

  constructor(
    private sessionStorage: SessionStorageService
  ) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.insurance = this.sessionStorage.retrieve('selectedResult');
    this.customerData = this.sessionStorage.retrieve('customerInfo');
    this.coverageData = this.sessionStorage.retrieve('coverageInfo');
    this.carData = this.sessionStorage.retrieve('carSearch');

    console.log(history.state.payload);
  }

}
