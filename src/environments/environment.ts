// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDGY_pglg5If6AGxjYrpqGikSCGz1J5TiU",
    authDomain: "iwow-dev.firebaseapp.com",
    databaseURL: "https://iwow-dev.firebaseio.com",
    projectId: "iwow-dev",
    storageBucket: "iwow-dev.appspot.com",
    messagingSenderId: "819617667228",
    appId: "1:819617667228:web:76a1c1da02aba8e44ab559",
    measurementId: "G-VKLDZGFGCK"
  },
  apiUrl: "http://161.202.184.162:8080/v1"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
