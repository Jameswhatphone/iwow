export const environment = {
  production: true,
  // Now use dev environment
  firebase: {
    apiKey: "AIzaSyCTinAjXwWBavBEu-_9IBk4cgIj0ppDsHQ",
    authDomain: "iwow-460fc.firebaseapp.com",
    databaseURL: "https://iwow-460fc.firebaseio.com",
    projectId: "iwow-460fc",
    storageBucket: "iwow-460fc.appspot.com",
    messagingSenderId: "119720762508",
    appId: "1:119720762508:web:c5c7c9c8f72bc460fcefa9",
    measurementId: "G-2Y4B1GXBR2"
  },
  apiUrl: "https://api.iwowthailand.com/v1"
};
